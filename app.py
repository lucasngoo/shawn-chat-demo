import os
from flask import Flask, render_template
from flask.ext.assets import Environment, Bundle

app = Flask(__name__)
env = Environment(app)


# print(os.path.join(os.path.dirname(os.path.abspath(__file__)), 'bower_components'))

env.load_path = [
  # look for libraries in bower_components in project root
  os.path.join(os.path.dirname(os.path.abspath(__file__)), 'bower_components'),
  os.path.join(os.path.dirname(os.path.abspath(__file__)), 'static')
]

# use this for any necessary scripts that MUST be loaded before the body tag.
js_head = Bundle(
  'foundation/js/vendor/modernizr.js',
  filters='rjsmin', output='gen/setup.min.js'
)

# these are loaded after the body tag
js = Bundle(
  'jquery/dist/jquery.js', 
  'underscore/underscore.js',
  'backbone/backbone.js', 
  'backbone.localStorage/backbone.localStorage.js',
  'marionette/lib/backbone.marionette.js',
  'foundation/js/vendor/fastclick.js',
  'foundation/js/foundation.js',
  'moment/moment.js',
  'js/app.js',
  'js/layouts.js',
  'js/*/*.js', 
  filters='rjsmin', output='gen/carousell.min.js'
)

css = Bundle(
  Bundle(
    'foundation/css/normalize.css', 
    'foundation/css/foundation.css',
    'font-awesome/css/font-awesome.css'
  ),
  Bundle(
    'css/*.styl',
    filters='stylus'
  ),
  filters='yui_css', output='gen/carousell.min.css'
)

env.register('js_head', js_head)
env.register('js_all', js)
env.register('css_all', css)

@app.route("/")
def index():
  return render_template('index.html')

if __name__ == "__main__":
    app.run(debug=True)