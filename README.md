Carousell chat app in Backbone + Flask
====

This is a single page application that is only accessible from '/'.

Structure of front-end js code (in /static/js):

1. /
  1. component folders (Eg. chat)
    1. the MVC code
  2. app.js - This is the bootstrap file that starts up the whole front end
  3. layouts.js - Region / Layouts that bind to the backbone structure

It should be fairly easy to extend the app to cover more components. Styling is minimal and adheres to the Foundation 5 responsive grid.

Backend code is configured to serve the initial HTML on '/', then allow the front-end to take over the full rendering of the chat app. Minification + concatenation is done through flask-assets. I use Stylus preprocessor to make my job easier for CSS. 