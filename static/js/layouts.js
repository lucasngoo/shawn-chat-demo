App.module('Layouts', function (Layouts, App, Backbone, Marionette, $, _){
  // prevent starting with parent
  // this.startWithParent = false;

  // layouts can nest other layouts as well. 
  // and contain multiple regions for rendering
  // separate regions for separate purposes
  Layouts.Centered = Backbone.Marionette.LayoutView.extend({
    template: "#layout-centered-tmpl",
    regions: {
      "chat": "#content"
    }
  });

  // can specify other layouts for other portions of the app... like a grid layout?
})