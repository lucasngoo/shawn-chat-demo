App = new Marionette.Application();

// for now only have 1 region. can extend to add other components like header, footer
App.addRegions({
  main: "#main"
});

// history API is good for Single page apps if we wanna extend functionality to cover more routes
App.on("start", function(){
  if (Backbone.history){    
    Backbone.history.start({pushState: true});
  }
});

// hardcode a login user 
// for differentiating between 'me' and another user
App.userId = '1238cabsdja'

$(function(){
  App.start();
})