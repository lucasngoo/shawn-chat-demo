App.module("Chat.Views", function (Views, App, Backbone, Marionette, $, _){

  // private views, not meant to be called directly
  var MessageView = Backbone.Marionette.ItemView.extend({
    template: "#message-tmpl",
    className: 'line',
    onBeforeRender: function(){
      this.$el.addClass(this.model.get('createdBy') == App.userId ? 'me' : 'not-me');
    },
    onRender: function(){
      var that = this;
      setTimeout(function(){
        that.$el.addClass('fade-in');
      }, 1);
    },
    templateHelpers: {
      prettyTime: function(){
        return moment(this.createdAt).calendar()
      }
    }
  });

  var NoMessagesView = Backbone.Marionette.ItemView.extend({
    template: '#no-messages-tmpl'
  })

  // Public Composite View that the router can render for instance..
  Views.Convo = Backbone.Marionette.CompositeView.extend({
    template: '#chat-convo-tmpl',
    className: 'chat-convo',
    childView: MessageView,
    emptyView: NoMessagesView,
    childViewContainer: '.messages',
    onRender: function(){
      this.changeButtonStatus();
    },
    onBeforeRender: function(){
      // make this full height
      this.$el.height($("#main").parent().height()); 
    },
    events: {
      "submit .message-form": "addMessage",
      "keyup input": "changeButtonStatus",
      "cut input": "changeButtonStatus",
      "paste input": "changeButtonStatus",
      "change input": "changeButtonStatus"
    },
    changeButtonStatus: function(e){
      if(!e || $(e.target).val().length == 0){
        this.$el.find('button').attr('disabled', true);
      }else{
        this.$el.find('button').attr('disabled', false);
      }
    },
    addMessage: function(e){
      e.preventDefault();

      this.collection.create({
        text: this.$el.find('.enter-message').val().trim(),
        createdBy: App.userId,
        createdAt: new Date()
      });

      this.$el.find('.message-form').trigger('reset'); // clears the form
      this.changeButtonStatus();
    },
    collectionEvents: {
      "add": "autoScroll"
    },
    autoScroll: function(){
      $messages = this.$el.find('.messages');
      // auto scroll down message container when you add a new message
      $messages.animate({
          scrollTop: $messages[0].scrollHeight
      }, 1000);

    }
  });
});