App.module('Chat', function (Chat, App, Backbone, Marionette, $, _){
  // prevent starting with parent
  // this.startWithParent = false;

  var controller = {
    list: function(){
      // prep layout
      var layout = new App.Layouts.Centered();
      // prep collection
      var messages = new Chat.Models.Messages(); 
      // prep view, pass collection to view
      var convo = new Chat.Views.Convo({
        collection: messages
      });
      // render the layout
      App.main.show(layout);
      // render the chat conversation inside the layout
      layout.chat.show(convo);
    }
  }

  var Router = Marionette.AppRouter.extend({
    controller: controller,
    appRoutes: {
      "": "list"
    }
  })

  Chat.addInitializer(function(){
    console.log('chat started')
    new Router({
      controller: controller
    });
  })
})