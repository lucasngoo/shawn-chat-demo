App.module("Chat.Models", function (Models, App, Backbone, Marionette, $, _){
  // for models and collections of the class Item
  var Message = Backbone.Model.extend({
    defaults:{
      createdAt: new Date(),
      createdBy: App.userId,
      convoId: '123'
    },
    validate: function(attrs){
      if(attrs.text.length == 0){
        return 'You have to type something';
      }
    }
  });

  // For usage if I want to create multiple distinct conversations 
  // Eg:
  /* 
      select a convo:
       |  [convo 1]  
       |  [convo 2]
       |  [convo 3]

  */
  // var Convo = Backbone.Model.extend({});

  Models.Messages = Backbone.Collection.extend({
    model: Message,
    url: '/api/messages',
    localStorage: new Backbone.LocalStorage('messages'),
    initialize: function(){
      that = this;
      that.fetch({
        success: function(collection, response, options){
          // if collection is empty - add some fixtures
          if(that.length == 0){
            that.create({text: 'Accepted Offer RM300', createdBy: App.userId, convoId: 123, createdAt: moment().subtract(5, 'm').toDate()});
            that.create({text: 'Let\'s meet to deal!\nOne north MRT tomorrow 7pm?', createdBy: App.userId, convoId: 123, createdAt: moment().subtract(3, 'm').toDate()});
            that.create({text: 'Ok see you!', createdBy: '12121se22xc', convoId: 123, createdAt: moment().toDate()});
          }
        },
        error: function(collection, response, options){
          
        }
      })
    }
  });
});